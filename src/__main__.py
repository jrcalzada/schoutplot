#!/usr/bin/env python
"""
MPI program to generate movies out of binary SCHISM outputs.
"""
from datetime import timedelta
from functools import partial
from math import ceil, sqrt
from pathlib import Path
import argparse
import hashlib
import json
import logging
import os
import pickle
import shutil
import sys
import tempfile

# from mpi4py import MPI
# from mpi4py.futures import MPICommExecutor
from PIL import Image
from colored_traceback.colored_traceback import Colorizer
from matplotlib.cm import ScalarMappable
from matplotlib.collections import LineCollection
from matplotlib.colors import Normalize
from matplotlib.lines import Line2D
from matplotlib.ticker import FixedLocator, FixedFormatter
from matplotlib.transforms import Bbox
from matplotlib.tri import Triangulation
from mpl_toolkits.axes_grid1 import make_axes_locatable
from pyproj import CRS
from scipy.interpolate import griddata
from scipy.spatial import KDTree
from shapely.geometry import LineString, Point, Polygon
import contextily as cx
import cv2
import geopandas as gpd
import matplotlib.pyplot as plt
import natsort as ns
import numpy as np
import pexpect
import seaborn as sns
import xarray as xr
import yaml


logger = logging.getLogger(__name__)


def mpiabort_excepthook(type, value, traceback):
    Colorizer('default').colorize_traceback(type, value, traceback)
    MPI.COMM_WORLD.Abort(-1)


def init_logger(log_level: str):
    logging.basicConfig(
        format="[%(asctime)s] %(name)s %(levelname)s: %(message)s",
        force=True,
        # datefmt="%Y-%m-%d %H:%M:%S "
    )
    log_level = {
            "warning": logging.WARNING,
            "info": logging.INFO,
            "debug": logging.DEBUG,
            "critical": logging.CRITICAL,
            "notset": logging.NOTSET,
        }[str(log_level).lower()]
    logger.setLevel(log_level)


def compile_read_output7_allnodes(output_path):
    logger.info(f"Compiling Fortran executable to {output_path}")
    ifort_compile = [
        "ifort",
        "-Bstatic",
        "-O2",
        "-assume",
        "byterecl",
        "-o",
        f"{output_path.name}",
        f"{Path(__file__).parent}/read_output7_allnodes.f90",
        f"{Path(__file__).parent}/compute_zcor.f90",
    ]
    with pexpect.spawn(
            ' '.join(ifort_compile),
            encoding='utf-8',
            timeout=None,
            cwd=str(output_path.parent),
            ) as p:
        p.logfile_read = sys.stdout
        p.expect(pexpect.EOF)

    if p.exitstatus != 0:
        raise Exception(p.before)


def get_read_output7_allnode_executable_path(comm, args):
    read_output7_allnodes_path = args.cache_directory / 'read_output7_allnodes'
    if comm.Get_rank() == 0:
        if not read_output7_allnodes_path.exists():
            compile_read_output7_allnodes(read_output7_allnodes_path)
    comm.barrier()
    return read_output7_allnodes_path


def convert_binary_output_to_npy(executable, args, file_to_read_from, fname):
    _work_directory = tempfile.TemporaryDirectory(
            dir=args.cache_directory
            )
    work_directory = Path(_work_directory.name)
    outputs_directory = args.outputs_directory
    stack_number, input_file_to_read_from = file_to_read_from.name.split('_')
    os.symlink(
        (outputs_directory / f"1_{input_file_to_read_from}").resolve(),
        work_directory / f"1_{input_file_to_read_from}",
    )
    if (outputs_directory / "vgrid.in").is_file():
        os.symlink(
                (outputs_directory / "vgrid.in").resolve(),
                work_directory / "vgrid.in"
                )
    elif (outputs_directory.parent / "vgrid.in").is_file():
        os.symlink(
            (outputs_directory.parent / "vgrid.in").resolve(),
            work_directory / "vgrid.in"
        )
    else:
        raise IOError(
            f"No vgrid.in found on {outputs_directory.parent} or {outputs_directory}."
        )
    if int(stack_number) != 1:
        os.symlink(
            file_to_read_from.resolve(),
            work_directory / file_to_read_from.name,
        )
    with pexpect.spawn(
        str(executable.resolve()),
        encoding="utf-8",
        cwd=work_directory,
        timeout=None,
    ) as p:
        p.logfile_read = sys.stdout
        p.expect("Input file to read from")
        p.sendline(f"{input_file_to_read_from}")
        p.expect("Input start and end stack")
        p.sendline(f"{stack_number} {stack_number}")
        p.expect(pexpect.EOF)
    if p.exitstatus != 0:
        raise Exception(p.before)

    lines = p.before.split("\n")
    for line in lines:
        if "nrec" in line:
            nrec = int(line.split("=")[-1].strip())
            break
    with open(work_directory / "extract.out") as f:
        j = len(f.readline().split())
    fp_mmap = np.lib.format.open_memmap(
            fname,
            mode='w+',
            dtype="float32",
            shape=(nrec, j)
            )
    with open(work_directory / "extract.out") as f:
        for k, line in enumerate(f):
            logger.debug(f"Processor {stack_number} flushing frame {k}.")
            fp_mmap[k, :] = np.array(list(map(float, line.strip("\n").split())))
            fp_mmap.flush()
    del fp_mmap
    # to read back do: mmapped_array = np.load(filename, mmap_mode='r')
    logger.debug(f"Done {stack_number}.")


def init_SCHISM_binary_to_npy(comm, args):
    executable = get_read_output7_allnode_executable_path(comm, args)
    files_to_process = ns.natsorted(args.outputs_directory.glob(f'*_{args.variable}.6*'), alg=ns.PATH)
    npy_directory = args.cache_directory / f'npy/{args.variable}/surface'
    npy_directory.mkdir(exist_ok=True, parents=True)
    npy_files = [npy_directory / f"{files_to_process.name}.npy" for files_to_process in files_to_process]
    with MPICommExecutor(comm, root=0) as executor:
        if executor is not None:
            iterable = ((
                executable,
                args,
                file_to_read_from,
                npy_file,
                ) for file_to_read_from, npy_file in zip(files_to_process, npy_files)
                if not npy_file.exists()
                )
            executor.starmap(convert_binary_output_to_npy, iterable)
    return npy_files


def generate_surface_frame(
    file_to_read_from,
    row_to_read_from,
    tri,
    topobathy,
    output_directory,
    cmap,
    levels,
    alpha,
    global_vmin,
    global_vmax,
    vmin,
    vmax,
    basemap_zoom,
    dpi,
    figsize,
    stations_data,
    bbox,
    crs,
    context,
    # use_inundation_depth: bool,
) -> Path:

    file_to_read_from = Path(file_to_read_from)
    logger.debug(
        f"Processing file {file_to_read_from.name} frame {row_to_read_from+1}.",
        # flush=True
    )
    fp_mmap = np.load(f'{file_to_read_from}', mmap_mode='r')
    time = timedelta(days=float(fp_mmap[row_to_read_from, 0]))
    data = np.array(fp_mmap[row_to_read_from, 1:])
    del fp_mmap

    # if use_inundation_depth is True:
    #     flood_idxs = np.where(topobathy > 0)
    #     data[flood_idxs] = np.subtract(data[flood_idxs], topobathy[flood_idxs])

    if context is not None:
        sns.set_context(context)

    if bbox is not None:
        used_indexes = np.unique(tri.triangles)
        node_indexes = np.arange(tri.x.shape[0])
        xeps = 0.1*(bbox.xmax - bbox.xmin)
        yeps = 0.1*(bbox.ymax - bbox.ymin)
        vert2_mask = ~np.logical_and(
            np.logical_and(bbox.xmin - xeps < tri.x, bbox.xmax + xeps > tri.x),
            np.logical_and(bbox.ymin - yeps < tri.y, bbox.ymax + yeps > tri.y)
        )
        tria3_mask = np.any(vert2_mask[tri.triangles], axis=1)
        del vert2_mask
        tria3_index = tri.triangles[~tria3_mask, :].flatten()
        for idx in reversed(np.where(~np.isin(node_indexes, used_indexes))[0]):
            tria3_index[np.where(tria3_index >= idx)] -= 1
        tria3_IDtag = np.arange(tri.triangles.shape[0]).take(np.where(~tria3_mask)[0])
        del tria3_mask
        tria3_index = tria3_index.reshape((tria3_IDtag.shape[0], 3))
        vert2_idxs = np.where(np.isin(node_indexes, used_indexes))[0]
        del used_indexes, node_indexes
        x = tri.x.take(vert2_idxs, axis=0)
        y = tri.y.take(vert2_idxs, axis=0)
        data = data.flatten().take(vert2_idxs, axis=0)
        tri = Triangulation(x, y, tria3_index)
        data = np.ma.masked_where(topobathy.take(vert2_idxs, axis=0) > data, data)
        del x, y, tria3_index, vert2_idxs

    else:
        data = np.ma.masked_where(topobathy > data, data)

    tri.set_mask(np.any(data.mask[tri.triangles], axis=1))

    # if prop_cycle is not None:
    #     plt.rcParams['axes.prop_cycle'] = prop_cycle

    if vmin is None:
        vmin = np.min(data)

    if vmax is None:
        vmax = np.max(data)

    if levels is None:
        levels = 256

    if isinstance(levels, int):
        levels = np.linspace(vmin, vmax, levels)

    # By default, assume that global_vmin and global_mvax are larger that vmin/vmax.
    # This guarantess that extend becomes 'both' as default.
    if global_vmin is None and vmin is not None:
        global_vmin = vmin - np.finfo(np.float32).eps
    if global_vmax is None and vmax is not None:
        global_vmax = vmax + np.finfo(np.float32).eps

    if global_vmin == vmin and global_vmax == vmax:
        extend = 'neither'
    elif global_vmin < vmin and global_vmax == vmax:
        extend = 'min'
    elif global_vmin == vmin and global_vmax > vmax:
        extend = 'max'
    elif global_vmin < vmin and global_vmax > vmax:
        extend = 'both'

    plt.tricontourf(
        tri,
        data,
        cmap=cmap,
        levels=levels,
        alpha=alpha,
        vmin=vmin,
        vmax=vmax,
        extend=extend,
    )

    plt.gca().axis('scaled')

    if bbox is not None:
        plt.gca().set_xlim(left=bbox.xmin, right=bbox.xmax)
        plt.gca().set_ylim(bottom=bbox.ymin, top=bbox.ymax)

    if dpi is not None:
        plt.gcf().set_dpi(dpi)

    if figsize is not None:
        plt.gcf().set_size_inches(*figsize)
    else:
        # let's make the default figure size 1920x108 with tight layout
        DPI = plt.gcf().get_dpi()
        width_inches = 1920 / DPI
        height_inches = 1080 / DPI
        plt.gcf().set_size_inches(width_inches, height_inches)

    basemap_kwargs = {"crs": crs}
    if basemap_zoom is not None:
        basemap_kwargs["zoom"] = basemap_zoom

    cx.add_basemap(
        plt.gca(),
        source="https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}",
        **basemap_kwargs,
    )

    if stations_data is not None:
        for i, station_data in enumerate(stations_data):
            kwargs = station_data.copy()
            coords = kwargs.pop('coords')
            marker_data = kwargs.get('marker')
            if marker_data is None:
                plt.text(coords[0], coords[1], str(i + 1), horizontalalignment='center', verticalalignment='center')
            elif isinstance(marker_data, dict):
                if 'style' not in marker_data:
                    plt.scatter(*coords, **marker_data)
                else:
                    if marker_data['style'] in Line2D.markers:
                        plt.scatter(*coords, **marker_data)
                    else:
                        plt.text(coords[0], coords[1], marker_data.pop('style'), **marker_data)
            elif marker_data in Line2D.markers:
                plt.scatter(*coords, **kwargs)
            elif isinstance(marker_data, (int, float, str)):
                plt.text(coords[0], coords[1], str(marker_data))
            else:
                raise Exception(f'unhandled marker data: {marker_data}')

    plt.title(str(time).split(".")[0])
    divider = make_axes_locatable(plt.gca())
    cax = divider.new_vertical(size="5%", pad=0.5, pack_start=True)
    plt.gcf().add_axes(cax)
    mappable = ScalarMappable(cmap=cmap)
    mappable.set_clim(vmin, vmax)
    cbar = plt.gcf().colorbar(
        mappable,
        cax=cax,
        orientation="horizontal",
        extend=extend,
    )
    # TODO: Hardcoded naming
    # if use_inundation_depth:
    #     cbar.set_label('Water elevation above ground. [m]')
    # else:
    cbar.set_label('Water elevation above datum. [m]')

    plt.gcf().set_tight_layout("tight")

    outdir = Path(output_directory) / f'{Path(file_to_read_from).stem}'
    outdir.mkdir(exist_ok=True, parents=True)
    outfile = outdir / f"{row_to_read_from+1:06d}.png"
    logger.debug(f"Saving to {outfile}")
    plt.savefig(
            outfile,
            bbox_inches='tight'
            )
    plt.close(plt.gcf())
    return outfile


def generate_combined_stations_frame(
        x,
        auto_stations_data,
        current_step,
        output_path,
        cmap=None,
        vmin=None,
        vmax=None,
        dpi=None,
        context='talk',
        ):

    if context is not None:
        sns.set_context(context)

    # fig, ax = plt.subplots()
    x = np.array([timedelta(days=float(_)).total_seconds() for _ in np.array(x).flatten()]).flatten()
    x.sort()

    num_stations = len(auto_stations_data)
    stations_per_column = int(ceil(sqrt(num_stations)))

    max_columns = -(-num_stations // stations_per_column)
    fig, axes = plt.subplots(nrows=stations_per_column, ncols=max_columns)
    axes = axes.flatten()

    if dpi is not None:
        fig.set_dpi(dpi)

    DPI = fig.get_dpi()
    width_inches = 1920 / DPI
    height_inches = 1080 / DPI
    fig.set_size_inches(width_inches, height_inches)
    plt.suptitle('Water surface elevation above geoid')
    plt.subplots_adjust(
            # hspace=0.3,
            wspace=0.1,
            left=0.05,
            right=1-0.05,
            top=1-0.08,
            bottom=0.05,
            )

    norm_kwargs = {}
    if vmin is not None:
        norm_kwargs["vmin"] = vmin

    if vmax is not None:
        norm_kwargs["vmax"] = vmax

    extra = {}
    if vmin is not None or vmax is not None:
        extra["norm"] = Normalize(**norm_kwargs)

    for i, auto_station_data in enumerate(auto_stations_data):
        full_ts = np.array(np.load(auto_station_data['npy'], mmap_mode='r'))
        points = np.array([x, full_ts]).T.reshape(-1, 1, 2)
        segments = np.concatenate([points[:-1], points[1:]], axis=1)
        axes[i].add_collection(
            LineCollection(
                segments,
                cmap=cmap,
                array=full_ts,
                # array=x,
                linewidth=1,
                **extra
            ))
        axes[i].fill_between(
            x[:current_step],
            full_ts[:current_step],
            step="mid",
            alpha=0.4,
            # cmap=cmap,
        )

        if 'title' in auto_station_data and auto_station_data['title'] is not None:
            axes[i].set_title(str(auto_station_data['title']))
        else:
            axes[i].set_title(f'Station {i+1}')

        if 'xlim' in auto_station_data:
            axes[i].set_xlim(*auto_station_data['xlim'])
        else:
            axes[i].set_xlim(x.min(), x.max())
    fig.savefig(
            output_path,
            # bbox_inches='tight'
            )
    plt.close(fig)
    logger.debug(f'Finished processing station slice: {output_path}')
    # return str(output_path)
    # if 'xlim' in station_data:
    #     plt.gca().set_xlim(*station_data['xlim'])
    # else:
    #     plt.gca().set_xlim(x.min(), x.max())
    # xlabels = []
    # for xpos, label in zip(*plt.xticks()):
    #     xlabels.append(str(timedelta(seconds=float(xpos))).split(".")[0])
    # # ax.set_xticklabels(xlabels)
    # plt.gca().xaxis.set_major_locator(FixedLocator(plt.xticks()[0]))  # Set FixedLocator with the x-tick positions
    # plt.gca().xaxis.set_major_formatter(FixedFormatter(xlabels))
    # plt.gca().set_xlabel('Time since model start [HH:MM:SS].')

    # if 'ylim' in station_data:
    #     plt.gca().set_ylim(*station_data['ylim'])
    # # else:
    # #     plt.gca().set_ylim(full_ts.min(), full_ts.max())
    # plt.gca().set_ylabel('Water elevation above datum [m].')

    # if dpi is not None:
    #     plt.gcf().set_dpi(dpi)

    # # if figsize is not None:
    # #     plt.gcf().set_size_inches(*figsize)
    # # else:
    #     # let's make the default figure size 1920x108 with tight layout
    # DPI = plt.gcf().get_dpi()
    # width_inches = 1920 / DPI
    # height_inches = 1080 / DPI
    # plt.gcf().set_size_inches(width_inches, height_inches)

    # plt.gcf().savefig(output_path, bbox_inches='tight')
    # plt.close(plt.gcf())
    # logger.debug(f'Finished processing station slice: {output_path}')
    # return str(output_path)

    # full_ts = np.load(station_data['npy'], mmap_mode='r')
    # points = np.array([x, full_ts]).T.reshape(-1, 1, 2)
    # segments = np.concatenate([points[:-1], points[1:]], axis=1)
    # # print(se)
    # # breakpoint()
    # extra = {}

    # norm_kwargs = {}
    # if vmin is not None:
    #     norm_kwargs["vmin"] = vmin

    # if vmax is not None:
    #     norm_kwargs["vmax"] = vmax

    # if vmin is not None or vmax is not None:
    #     extra["norm"] = Normalize(**norm_kwargs)

    # lc = LineCollection(
    #     segments,
    #     cmap=cmap,
    #     array=full_ts,
    #     # array=x,
    #     # linewidth=1,
    #     **extra
    # )
    # # lc.set_array(x)
    # plt.gca().add_collection(lc)

    # plt.fill_between(
    #     x[:current_step],
    #     full_ts[:current_step],
    #     step="mid",
    #     alpha=0.4,
    #     # cmap=cmap,
    # )
    # # s_kwargs = props.copy()
    # # coords = kwargs.pop('coords')
    # # marker_data = kwargs.get('marker')
    # # if marker_data is not None:
    # #     if isinstance(marker_data, dict):
    # #         if 'style' not in marker_data:
    # #             plt.scatter(*coords, **marker_data)
    # #         else:
    # #             if marker_data['style'] in Line2D.markers:
    # #                 plt.scatter(*coords, **marker_data)
    # #             else:
    # #                 plt.text(coords[0], coords[1], marker_data.pop('style'), **marker_data)
    # #     elif marker_data in Line2D.markers:
    # #         plt.scatter(*coords, **kwargs)
    # #     elif isinstance(marker_data, (int, float, str)):
    # #         plt.text(coords[0], coords[1], str(marker_data))
    # #     else:
    # #         raise Exception(f'unhandled marker data: {marker_data}')
    # # print(props)
    # # exit()
    # # if 'marker' not in props:
    # #     props.update(marker='o')
    # # plt.plot(x[current_step], full_ts[current_step], linestyle='none')  # , **props)
    # # plt.scatter(x[current_step], full_ts[current_step], **props)
    # # kwargs = station_data.copy()
    # # kwargs.pop('coords')
    # # coords = (x[current_step], full_ts[current_step])
    # # marker_data = kwargs.get('marker')
    # # if marker_data is not None:
    # #     if isinstance(marker_data, dict):
    # #         if bool(marker_data.get('follow', False)) is True:
    # #             if 'style' not in marker_data:
    # #                 plt.scatter(*coords, **marker_data)
    # #             else:
    # #                 if marker_data['style'] in Line2D.markers:
    # #                     plt.scatter(*coords, **marker_data)
    # #                 else:
    # #                     plt.text(coords[0], coords[1], marker_data.pop('style'), **marker_data)
    # #     elif marker_data in Line2D.markers:
    # #         plt.scatter(*coords, **kwargs)
    # #     elif isinstance(marker_data, (int, float, str)):
    # #         plt.text(coords[0], coords[1], str(marker_data))
    # #     else:
    # #         raise Exception(f'unhandled marker data: {marker_data}')

    # if 'title' in station_data and station_data['title'] is not None:
    #     plt.title(str(station_data['title']))
    # else:
    #     plt.title(f'Station {station_number}')

    # if 'xlim' in station_data:
    #     plt.gca().set_xlim(*station_data['xlim'])
    # else:
    #     plt.gca().set_xlim(x.min(), x.max())
    # xlabels = []
    # for xpos, label in zip(*plt.xticks()):
    #     xlabels.append(str(timedelta(seconds=float(xpos))).split(".")[0])
    # # ax.set_xticklabels(xlabels)
    # plt.gca().xaxis.set_major_locator(FixedLocator(plt.xticks()[0]))  # Set FixedLocator with the x-tick positions
    # plt.gca().xaxis.set_major_formatter(FixedFormatter(xlabels))
    # plt.gca().set_xlabel('Time since model start [HH:MM:SS].')

    # if 'ylim' in station_data:
    #     plt.gca().set_ylim(*station_data['ylim'])
    # # else:
    # #     plt.gca().set_ylim(full_ts.min(), full_ts.max())
    # plt.gca().set_ylabel('Water elevation above datum [m].')

    # if dpi is not None:
    #     plt.gcf().set_dpi(dpi)

    # # if figsize is not None:
    # #     plt.gcf().set_size_inches(*figsize)
    # # else:
    #     # let's make the default figure size 1920x108 with tight layout
    # DPI = plt.gcf().get_dpi()
    # width_inches = 1920 / DPI
    # height_inches = 1080 / DPI
    # plt.gcf().set_size_inches(width_inches, height_inches)

    # plt.gcf().savefig(output_path, bbox_inches='tight')
    # plt.close(plt.gcf())
    # logger.debug(f'Finished processing station slice: {output_path}')
    # return str(output_path)


def generate_station_frame(
    x,
    station_data,
    station_number,
    current_step,
    output_path,
    cmap=None,
    vmin=None,
    vmax=None,
    dpi=None,
    context='talk',
) -> None:

    logger.debug(f'Processing station {station_data["npy"]} step {current_step=}')

    if context is not None:
        sns.set_context(context)

    # fig, ax = plt.subplots()
    x = np.array([timedelta(days=float(_)).total_seconds() for _ in np.array(x).flatten()]).flatten()
    x.sort()
    full_ts = np.load(station_data['npy'], mmap_mode='r')
    points = np.array([x, full_ts]).T.reshape(-1, 1, 2)
    segments = np.concatenate([points[:-1], points[1:]], axis=1)
    # print(se)
    # breakpoint()
    extra = {}

    norm_kwargs = {}
    if vmin is not None:
        norm_kwargs["vmin"] = vmin

    if vmax is not None:
        norm_kwargs["vmax"] = vmax

    if vmin is not None or vmax is not None:
        extra["norm"] = Normalize(**norm_kwargs)

    lc = LineCollection(
        segments,
        cmap=cmap,
        array=full_ts,
        # array=x,
        # linewidth=1,
        **extra
    )
    # lc.set_array(x)
    plt.gca().add_collection(lc)

    plt.fill_between(
        x[:current_step],
        full_ts[:current_step],
        step="mid",
        alpha=0.4,
        # cmap=cmap,
    )
    # s_kwargs = props.copy()
    # coords = kwargs.pop('coords')
    # marker_data = kwargs.get('marker')
    # if marker_data is not None:
    #     if isinstance(marker_data, dict):
    #         if 'style' not in marker_data:
    #             plt.scatter(*coords, **marker_data)
    #         else:
    #             if marker_data['style'] in Line2D.markers:
    #                 plt.scatter(*coords, **marker_data)
    #             else:
    #                 plt.text(coords[0], coords[1], marker_data.pop('style'), **marker_data)
    #     elif marker_data in Line2D.markers:
    #         plt.scatter(*coords, **kwargs)
    #     elif isinstance(marker_data, (int, float, str)):
    #         plt.text(coords[0], coords[1], str(marker_data))
    #     else:
    #         raise Exception(f'unhandled marker data: {marker_data}')
    # print(props)
    # exit()
    # if 'marker' not in props:
    #     props.update(marker='o')
    # plt.plot(x[current_step], full_ts[current_step], linestyle='none')  # , **props)
    # plt.scatter(x[current_step], full_ts[current_step], **props)
    # kwargs = station_data.copy()
    # kwargs.pop('coords')
    # coords = (x[current_step], full_ts[current_step])
    # marker_data = kwargs.get('marker')
    # if marker_data is not None:
    #     if isinstance(marker_data, dict):
    #         if bool(marker_data.get('follow', False)) is True:
    #             if 'style' not in marker_data:
    #                 plt.scatter(*coords, **marker_data)
    #             else:
    #                 if marker_data['style'] in Line2D.markers:
    #                     plt.scatter(*coords, **marker_data)
    #                 else:
    #                     plt.text(coords[0], coords[1], marker_data.pop('style'), **marker_data)
    #     elif marker_data in Line2D.markers:
    #         plt.scatter(*coords, **kwargs)
    #     elif isinstance(marker_data, (int, float, str)):
    #         plt.text(coords[0], coords[1], str(marker_data))
    #     else:
    #         raise Exception(f'unhandled marker data: {marker_data}')

    if 'title' in station_data and station_data['title'] is not None:
        plt.title(str(station_data['title']))
    else:
        plt.title(f'Station {station_number}')

    if 'xlim' in station_data:
        plt.gca().set_xlim(*station_data['xlim'])
    else:
        plt.gca().set_xlim(x.min(), x.max())
    xlabels = []
    for xpos, label in zip(*plt.xticks()):
        xlabels.append(str(timedelta(seconds=float(xpos))).split(".")[0])
    # ax.set_xticklabels(xlabels)
    plt.gca().xaxis.set_major_locator(FixedLocator(plt.xticks()[0]))  # Set FixedLocator with the x-tick positions
    plt.gca().xaxis.set_major_formatter(FixedFormatter(xlabels))
    plt.gca().set_xlabel('Time since model start [HH:MM:SS].')

    if 'ylim' in station_data:
        plt.gca().set_ylim(*station_data['ylim'])
    # else:
    #     plt.gca().set_ylim(full_ts.min(), full_ts.max())
    plt.gca().set_ylabel('Water elevation above datum [m].')

    if dpi is not None:
        plt.gcf().set_dpi(dpi)

    # if figsize is not None:
    #     plt.gcf().set_size_inches(*figsize)
    # else:
        # let's make the default figure size 1920x108 with tight layout
    DPI = plt.gcf().get_dpi()
    width_inches = 1920 / DPI
    height_inches = 1080 / DPI
    plt.gcf().set_size_inches(width_inches, height_inches)

    plt.gcf().savefig(output_path, bbox_inches='tight')
    plt.close(plt.gcf())
    logger.debug(f'Finished processing station slice: {output_path}')
    return str(output_path)


def init_hgrid(comm, args, hgrid_pkl_path):
    if comm.Get_rank() == 0 and not hgrid_pkl_path.is_file():
        logger.info("Begin reading hgrid file...")
        hgrid = Hgrid.open(args.outputs_directory.parent / 'hgrid.ll')
        logger.info("Done reading hgrid file.")
        with open(hgrid_pkl_path, "wb") as handle:
            pickle.dump(hgrid, handle)
    comm.barrier()


def load_hgrid_from_args(comm, args):
    hgrid_pkl_path = args.cache_directory / 'hgrid.pkl'
    if not hgrid_pkl_path.is_file():
        init_hgrid(comm, args, hgrid_pkl_path)
    with open(hgrid_pkl_path, "rb") as handle:
        return pickle.load(handle)


def get_vmin_from_npy(fname):
    return np.min(np.load(fname, mmap_mode='r')[:, 1:])


def get_vmax_from_npy(fname):
    return np.max(np.load(fname, mmap_mode='r')[:, 1:])


def init_global_vmin_vmax(comm, args, npy_filenames):
    global_vmin = float('inf')
    global_vmax = -float('inf')
    with MPICommExecutor(comm) as executor:
        if executor is not None:
            global_vmin = np.min(list(executor.map(get_vmin_from_npy, npy_filenames)))
            global_vmax = np.max(list(executor.map(get_vmax_from_npy, npy_filenames)))
    global_vmin = comm.bcast(global_vmin, root=0)
    global_vmax = comm.bcast(global_vmax, root=0)
    return global_vmin, global_vmax


def init_surface_frames(comm, hgrid, args, filenames, stations):
    global_vmin, global_vmax = init_global_vmin_vmax(comm, args, filenames)
    with MPICommExecutor(comm, root=0) as executor:
        if executor is not None:
            if hasattr(hgrid.elements, 'triangulation'):
                del hgrid.elements.triangulation
            tri = hgrid.elements.triangulation
            job_args = []
            for fname in filenames:
                nrows = np.load(f'{fname.resolve()}', mmap_mode='r').shape[0]
                for row_to_read_from in range(nrows):
                    job_args.append((
                            f'{fname.resolve()}',
                            row_to_read_from,
                            tri,
                            hgrid.values,
                            args.frames_directory,
                            args.cmap,
                            args.levels,
                            args.alpha,
                            global_vmin,
                            global_vmax,
                            args.vmin,
                            args.vmax,
                            args.basemap_zoom,
                            args.dpi,
                            args.figsize,
                            stations,
                            args.bbox,
                            CRS.from_epsg(4326),
                            args.context,
                            # args.use_inundation_depth,
                        ))
            surface_frame_files = list(executor.starmap(generate_surface_frame, job_args))
        else:
            surface_frame_files = None
    return comm.bcast(surface_frame_files, root=0)


def get_auto_station_coords(auto_station_request, hgrid, bbox):

    tri = hgrid.elements.triangulation
    data = hgrid.values
    if bbox is not None:
        used_indexes = np.unique(tri.triangles)
        node_indexes = np.arange(tri.x.shape[0])
        xeps = 0.1*(bbox.xmax - bbox.xmin)
        yeps = 0.1*(bbox.ymax - bbox.ymin)
        vert2_mask = ~np.logical_and(
            np.logical_and(bbox.xmin - xeps < tri.x, bbox.xmax + xeps > tri.x),
            np.logical_and(bbox.ymin - yeps < tri.y, bbox.ymax + yeps > tri.y)
        )
        tria3_mask = np.any(vert2_mask[tri.triangles], axis=1)
        del vert2_mask
        tria3_index = tri.triangles[~tria3_mask, :].flatten()
        for idx in reversed(np.where(~np.isin(node_indexes, used_indexes))[0]):
            tria3_index[np.where(tria3_index >= idx)] -= 1
        tria3_IDtag = np.arange(tri.triangles.shape[0]).take(np.where(~tria3_mask)[0])
        del tria3_mask
        tria3_index = tria3_index.reshape((tria3_IDtag.shape[0], 3))
        vert2_idxs = np.where(np.isin(node_indexes, used_indexes))[0]
        del used_indexes, node_indexes
        x = tri.x.take(vert2_idxs, axis=0)
        y = tri.y.take(vert2_idxs, axis=0)
        data = data.flatten().take(vert2_idxs, axis=0)
        tri = Triangulation(x, y, tria3_index)
        data = np.ma.masked_where(data.take(vert2_idxs, axis=0) > data, data)
        del x, y, tria3_index, vert2_idxs

    contour_level = auto_station_request['contour']
    original_backend = plt.get_backend()
    plt.switch_backend('agg')
    ax = plt.tricontour(tri, data, levels=[contour_level])
    plt.close(plt.gcf())
    plt.switch_backend(original_backend)
    linestrings = []
    for path_collection in ax.collections:
        for path in path_collection.get_paths():
            linestrings.append(LineString(path.vertices))
    max_length_linestring = max(linestrings, key=lambda ls: ls.length)
    num_points = auto_station_request['stations']
    points = []
    for i in range(1, num_points + 1):
        fraction = i / (num_points + 1)
        point = max_length_linestring.interpolate(fraction, normalized=True)
        points.append(point)
    return points


def get_auto_stations_requests(args):
    if args.auto_stations is None:
        return
    auto_stations_file = args.output_directory / args.auto_stations.name
    if not args.auto_stations.resolve() == auto_stations_file.resolve():
        shutil.copy2(args.auto_stations, auto_stations_file)
    with open(auto_stations_file) as fh:
        auto_stations_request = yaml.load(fh, Loader=yaml.SafeLoader)
    if 'auto_stations' not in auto_stations_request:
        raise ValueError(f'File {auto_stations_file} must contain an auto_stations key.')
    auto_stations_requests = auto_stations_request['auto_stations']
    if isinstance(auto_stations_requests, dict):
        auto_stations_requests = [auto_stations_requests]
    processed_requests = []
    for request in auto_stations_requests:
        # Check if contour key exists in the dictionary
        if "contour" not in request:
            raise ValueError("Each item must contain the 'contour' key")
        # Set default value for stations key
        stations = request.get("stations", 1)
        # Check if stations value is valid
        if not isinstance(stations, int) or stations <= 0:
            raise ValueError("The 'stations' key must be an integer greater than 0")
        # Check for optional marker and color entries, set default values
        markers = request.get("marker", None)
        colors = request.get("color", None)

        # Cast marker and color to list if not a list and not None
        if markers is not None:
            if not isinstance(markers, list):
                markers = [markers] * stations
        else:
            markers = [None] * stations

        if colors is not None:
            if not isinstance(colors, list):
                colors = [colors] * stations
        else:
            colors = [None] * stations
        # Check if marker and color lists have the same length as stations
        if len(markers) != stations or len(colors) != stations:
            raise ValueError("The 'marker' and 'color' lists must have the same length as the 'stations' value")
        # Create a processed request dictionary
        processed_request = {
            "contour": request["contour"],
            "stations": stations,
            "marker": markers,
            "color": colors,
            "title": request.get("title", None)
        }
        processed_requests.append(processed_request)
    return processed_requests


def init_auto_stations_coords(comm, hgrid, args, auto_stations_requests):
    ncolors = comm.Get_size()
    color = comm.Get_rank()
    local_auto_stations_coords = []
    for i, auto_station_request in enumerate(auto_stations_requests):
        if i % ncolors == color:
            local_auto_stations_coords.append(get_auto_station_coords(auto_station_request, hgrid, args.bbox))
    gathered_auto_stations_coords = comm.allgather(local_auto_stations_coords)
    if comm.Get_rank() == 0:
        final_all_auto_stations_coords = [coord for sublist in gathered_auto_stations_coords for coord in sublist]
    else:
        final_all_auto_stations_coords = None
    return comm.bcast(final_all_auto_stations_coords, root=0)


def find_element_index_for_station(hgrid, station_coords):
    hgrid_coords = np.array(hgrid.coords)
    hgrid_elements = np.array(hgrid.elements.array)

    idxs = KDTree(hgrid_coords).query(station_coords)[1]
    eidxs = np.where(np.any(np.isin(hgrid_elements, idxs), axis=1))[0]
    relevant_elements = hgrid_elements[eidxs]

    polygons = []
    for element in relevant_elements:
        if element[3] == -1:  # Triangle
            polygon_coords = hgrid_coords[element[:3].astype(int)]
        else:  # Quad
            polygon_coords = hgrid_coords[element.astype(int)]

        polygons.append(Polygon(polygon_coords))

    gdf = gpd.GeoDataFrame({"element_index": eidxs}, geometry=polygons)

    station_point = Point(station_coords)
    containing_polygon = gdf[gdf.geometry.contains(station_point)]

    if len(containing_polygon) > 0:
        return int(containing_polygon.iloc[0].element_index)
    else:
        return None  # station_coords not found in any element


def init_auto_stations_element_index(comm, hgrid, auto_stations_data):
    ncolors = comm.Get_size()
    color = comm.Get_rank()
    local_auto_stations_tri_index = []
    for i, auto_station_data in enumerate(auto_stations_data):
        if i % ncolors == color:
            local_auto_stations_tri_index.append(
                    find_element_index_for_station(
                        hgrid,
                        auto_station_data['coords']
                        )
                    )
    gathered_auto_stations_tri_idxs = comm.allgather(local_auto_stations_tri_index)
    if comm.Get_rank() == 0:
        final_auto_stations_tri_idxs = [coord for sublist in gathered_auto_stations_tri_idxs for coord in sublist]
    else:
        final_auto_stations_tri_idxs = None
    return comm.bcast(final_auto_stations_tri_idxs, root=0)


def init_auto_stations_data(comm, hgrid, args, npy_filenames):
    if comm.Get_rank() == 0:
        auto_stations_requests = get_auto_stations_requests(args)
    else:
        auto_stations_requests = None
    comm.barrier()
    auto_stations_requests = comm.bcast(auto_stations_requests, root=0)
    if auto_stations_requests is None:
        return
    auto_stations_coords = init_auto_stations_coords(comm, hgrid, args, auto_stations_requests)
    auto_stations_data = []
    for i, auto_station in enumerate(auto_stations_requests):
        for j in range(auto_station['stations']):
            coords = np.array(auto_stations_coords[i][j].coords)[0].tolist()
            marker = auto_station['marker'][j] if j < len(auto_station['marker']) else None
            color = auto_station['color'][j] if j < len(auto_station['color']) else None
            auto_station_data = {
                    'coords': coords,
                    'marker': marker,
                    'color': color,
                    'title': auto_station.get('title', None)
                    }
            auto_stations_data.append(auto_station_data)
    auto_stations_element_indexes = init_auto_stations_element_index(comm, hgrid, auto_stations_data)
    for i, eidx in enumerate(auto_stations_element_indexes):
        auto_stations_data[i]['eidx'] = eidx
        npyfile = args.cache_directory / (f'npy/{args.variable}/stations/' + hashlib.sha256(
            json.dumps(auto_stations_data[i]['coords']).encode('utf-8')).hexdigest() + '.npy')
        npyfile.parent.mkdir(exist_ok=True, parents=True)
        auto_stations_data[i]['npy'] = str(npyfile)

    with MPICommExecutor(comm, root=0) as executor:
        if executor is not None:
            for station_data in auto_stations_data:
                if not Path(station_data['npy']).exists():
                    eidxs = hgrid.elements.array[station_data['eidx']]
                    if np.any(np.ma.is_masked(eidxs)):
                        eidxs = eidxs[:3].data
                    else:
                        eidxs = eidxs.data
                    input_coords = hgrid.coords[eidxs]
                    job_args = []
                    for fname in npy_filenames:
                        nrows = np.load(f'{fname.resolve()}', mmap_mode='r').shape[0]
                        for row_to_read_from in range(nrows):
                            job_args.append((
                                station_data['coords'],
                                f'{fname.resolve()}',
                                row_to_read_from,
                                input_coords,
                                eidxs,
                                ))
                    np.save(
                        station_data['npy'],
                        np.array(list(executor.starmap(get_station_step, job_args)))
                        )
    return auto_stations_data


def get_station_step(station_coords, file_to_read_from, row_to_read_from, input_coords, node_indexes) -> float:
    input_values = np.load(file_to_read_from, mmap_mode='r')[row_to_read_from, node_indexes+1]
    return griddata(
            (input_coords[:, 0], input_coords[:, 1]),
            input_values,
            (station_coords[0], station_coords[1]),
            method='linear'
            )


def init_combined_stations_frames(comm, args, auto_stations_data, npy_filenames):
    stations_frames = []
    if auto_stations_data is None:
        return stations_frames
    with MPICommExecutor(comm) as executor:
        if executor is not None:
            x = []
            for fname in npy_filenames:
                x.extend(np.load(fname, mmap_mode='r')[:, 0].tolist())
            nsteps = len(x)
            job_args = []
            cache_directory = args.frames_directory / 'combined_stations'
            cache_directory.mkdir(exist_ok=True, parents=True)
            stations_frames = []
            for current_step in range(nsteps):
                output_path = cache_directory / f'{current_step+1:0{len(str(nsteps))}d}.png'
                job_args.append((
                        x,
                        auto_stations_data,
                        current_step,
                        output_path,
                        args.cmap,
                        # args.vmin,
                        None,
                        # args.vmax,
                        None,
                        args.dpi
                    ))
                stations_frames.append(output_path)
            list(executor.starmap(generate_combined_stations_frame, job_args))
    return comm.bcast(stations_frames, root=0)


def select_fourcc(file_name):
    file_extension = Path(file_name).suffix.lower()
    if file_extension == '.avi':
        return cv2.VideoWriter_fourcc(*'MJPG')
    elif file_extension in ['.mp4', '.mov']:
        return cv2.VideoWriter_fourcc(*'mp4v')
    else:
        raise ValueError(f"Unsupported file extension: {file_extension}")


def generate_combined_frame(surface_image_path, station_image_path, output_path):

    surface_image = Image.open(surface_image_path)
    station_image = Image.open(station_image_path)
    surf_width, height = surface_image.size
    station_width, station_height = station_image.size
    station_aspect_ratio = station_width / station_height

    new_station_height = height
    new_station_width = int(new_station_height * station_aspect_ratio)

    station_image = station_image.resize((new_station_width, new_station_height))

    # Compose the new image
    composed_image = Image.new("RGB", (surf_width + new_station_width, height))

    composed_image.paste(surface_image, (0, 0))
    composed_image.paste(station_image, (surf_width, 0))

    composed_image.save(output_path)
    return output_path


def init_combined_frames(
    comm,
    args,
    surface_frames,
    stations_frames,
):

    cache_directory = args.frames_directory / 'combined'
    cache_directory.mkdir(parents=True, exist_ok=True)

    # Adjust each station image height to match the surface image height and scale the width to maintain aspect ratio
    nsteps = len(stations_frames) if stations_frames is not None else 0
    if nsteps == 0:
        # short circuit if no stations
        return surface_frames
    output_paths = []
    with MPICommExecutor(comm) as executor:
        if executor is not None:
            job_args = []
            for i, (surface_image_path, station_image_path) in enumerate(zip(surface_frames, stations_frames)):
                output_path = cache_directory / f'{i+1:0{len(str(nsteps))}d}.png'
                job_args.append((
                    surface_image_path,
                    station_image_path,
                    output_path,
                    ))
                output_paths.append(output_path)
            list(executor.starmap(generate_combined_frame, job_args))
    return comm.bcast(output_paths, root=0)


def create_movie_from_frame_paths(output_filename, frame_list, fourcc=None, fps=6):
    frame = cv2.imread(str(frame_list[0]))
    height, width, channels = frame.shape

    # Define the codec using VideoWriter_fourcc and create a VideoWriter object
    fourcc = fourcc or select_fourcc(output_filename)
    out = cv2.VideoWriter(str(output_filename), fourcc, float(fps), (width, height))

    # Write the frames to the video file
    for frame_path in frame_list:
        frame = cv2.imread(str(frame_path))
        out.write(frame)

    # Release the VideoWriter
    out.release()


def init_combined_movie_tasks(comm, args, auto_stations_data, surface_frames, npy_filenames, tasks):
    # init the combined version:
    combined_stations_frames = init_combined_stations_frames(comm, args, auto_stations_data, npy_filenames)
    combined_frames = init_combined_frames(comm, args, surface_frames, combined_stations_frames)
    combined_movie_output_path = args.output_directory / f'combined.{args.extension}'
    tasks.append(
        partial(
            create_movie_from_frame_paths,
            combined_movie_output_path,
            combined_frames,
            fourcc=args.fourcc,
            fps=args.fps
            )
        )


def init_individual_stations_frames(comm, args, auto_stations_data, npy_filenames):
    stations_frames = []
    if auto_stations_data is None:
        return stations_frames
    with MPICommExecutor(comm) as executor:
        if executor is not None:
            x = []
            for fname in npy_filenames:
                x.extend(np.load(fname, mmap_mode='r')[:, 0].tolist())
            nsteps = len(x)
            for station_number, station_data in enumerate(auto_stations_data):
                cache_directory = args.frames_directory /\
                    f'individual_stations/{station_number+1:0{len(str(len(auto_stations_data)))}}'
                cache_directory.mkdir(exist_ok=True, parents=True)
                job_args = []
                stations_frames_group = []
                for current_step in range(nsteps):
                    output_path = cache_directory / f'{current_step+1:0{len(str(nsteps))}d}.png'
                    job_args.append((
                            x,
                            station_data,
                            station_number,
                            current_step,
                            output_path,
                            args.cmap,
                            # args.vmin,
                            None,
                            # args.vmax,
                            None,
                            args.dpi
                        ))
                    stations_frames_group.append(output_path)
                stations_frames.append(stations_frames_group)
                list(executor.starmap(generate_station_frame, job_args))
    return comm.bcast(stations_frames, root=0)


def init_individual_movie_tasks(comm, args, auto_stations_data, surface_frames, npy_filenames, tasks):
    standalone_surface_movie_output_path = args.output_directory / f'surface.{args.extension}'
    tasks.append(
        partial(
            create_movie_from_frame_paths,
            standalone_surface_movie_output_path,
            surface_frames,
            fourcc=args.fourcc,
            fps=args.fps
            )
        )
    # init individual stations plots
    individual_stations_frames = init_individual_stations_frames(comm, args, auto_stations_data, npy_filenames)
    for i, individual_station_group in enumerate(individual_stations_frames):
        individual_station_group_filename = args.output_directory /\
            f'station_{i+1:0{len(str(len(auto_stations_data)))}d}.mp4'
        tasks.append(
            partial(
                create_movie_from_frame_paths,
                individual_station_group_filename,
                individual_station_group,
                fourcc=args.fourcc,
                fps=args.fps
                )
            )


def entrypoint(args, comm=None):
    # comm = comm or MPI.COMM_WORLD

    files_to_process = ns.natsorted(args.run_directory.glob(f'schout_*.nc'), alg=ns.PATH)
    # print(files_to_process)
    ds = xr.open_dataset(files_to_process[0])
    # breakpoint()
    x = ds['SCHISM_hgrid_node_x'].values
    y = ds['SCHISM_hgrid_node_y'].values
    elements = ds['SCHISM_hgrid_face_nodes'].values  # x4
    # tria_rows = np.where(~np.isnan(elements[:, -1]))
    trias = elements[np.where(np.isnan(elements[:, -1])), :3].astype(int) - 1
    trias = trias[0, :]
    quads = elements[np.where(~np.isnan(elements[:, -1]))].astype(int) - 1
    split_quads = []
    for quad in quads:
        split_quads.append([quad[0], quad[1], quad[3]])
        split_quads.append([quad[1], quad[2], quad[3]])
    split_quads = np.array(split_quads)
    triangles = np.vstack([trias, split_quads])
    tri = Triangulation(x, y, triangles)
    frame_paths = []
    for file_path in files_to_process:
        ds = xr.open_dataset(file_path)
        for i, this_time in enumerate(ds.time):
            this_value_at_this_time = ds[args.variable].values[i, :, 0]
            mask = np.where(np.isnan(this_value_at_this_time))[0].astype(bool)
            tri.set_mask(np.any(mask[tri.triangles], axis=1))
            # this_value_at_this_time = this_value_at_this_time.data
            this_value_at_this_time[mask] = -99999.
            plt.tricontourf(tri, this_value_at_this_time, cmap='jet')
            plt.gca().axis('scaled')
            plt.show(block=True)
        # breakpoint()
        # frame_paths.append(generate_surface_frame( ... ))
        breakpoint()

    print("Done!")




    # npy_filenames = init_SCHISM_binary_to_npy(comm, args)
    # hgrid = load_hgrid_from_args(comm, args)
    # auto_stations_data = init_auto_stations_data(comm, hgrid, args, npy_filenames)
    # surface_frames = init_surface_frames(comm, hgrid, args, npy_filenames, auto_stations_data)
    # tasks = []
    # if args.mode == 'combined':
    #     init_combined_movie_tasks(comm, args, auto_stations_data, surface_frames, npy_filenames, tasks)
    # elif args.mode == 'individual':
    #     init_individual_movie_tasks(comm, args, auto_stations_data, surface_frames, npy_filenames, tasks)
    # elif args.mode == 'both':
    #     init_combined_movie_tasks(comm, args, auto_stations_data, surface_frames, npy_filenames, tasks)
    #     init_individual_movie_tasks(comm, args, auto_stations_data, surface_frames, npy_filenames, tasks)
    # else:
    #     raise NotImplementedError(f'Unhandled argument: {args.mode=}')
    # with MPICommExecutor(MPI.COMM_WORLD, root=0) as executor:
    #     if executor is not None:
    #         futures = []
    #         for task in tasks:
    #             futures.append(executor.submit(task))
    #         [future.result() for future in futures]


class RunDirectoryAction(argparse.Action):

    def __call__(self, parser, namespace, run_directory, option_string=None):

        if run_directory.name == 'outputs':
            outputs_directory = run_directory
            run_directory = run_directory.parent
        else:
            outputs_directory = run_directory / 'outputs'
        setattr(namespace, self.dest, run_directory)
        setattr(namespace, 'outputs_directory', outputs_directory)
        # _tmp_parser = argparse.ArgumentParser(add_help=False)
        # _tmp_parser.add_argument('run_directory', type=Path)
        # _tmp_parser.add_argument('variable')
        # _tmp_parser.add_argument('--output-directory', '-o', type=Path, required=True)
        # _tmp_parser.add_argument('--cache-directory', type=Path)
        # # setattr(namespace, 'cache_directory', known.output_directory.parent / '.tmp-tsunamiplot')
        # # namespace.cache_directory.mkdir(exist_ok=True, parents=True)
        # setattr(namespace, 'frames_directory', tempfile.TemporaryDirectory(dir=namespace.output_directory))
        # if namespace.frames_directory.exists():
        #     sh.rmtree(namespace.frames_directory)


class OutputDirectoryAction(argparse.Action):
    def __call__(self, parser, namespace, output_directory, option_string=None):
        _tmp_parser = argparse.ArgumentParser(add_help=False)
        _tmp_parser.add_argument('run_directory', type=Path)
        _tmp_parser.add_argument('variable')
        _tmp_parser.add_argument('--cache-directory', type=Path)
        _tmp_parser.add_argument('--extension', choices=['mp4', 'mov', 'avi'], default='mp4')
        known, _ = _tmp_parser.parse_known_args()
        if output_directory.name != known.variable:
            output_directory /= known.variable
        output_directory.mkdir(exist_ok=True, parents=True)
        setattr(namespace, self.dest, output_directory)
        # setattr(namespace, 'output_filename', output_directory / f'{known.variable}.{known.extension}')
        if known.cache_directory is None:
            setattr(namespace, 'cache_directory', output_directory / '.tmp-tsunamiplot')
            namespace.cache_directory.mkdir(exist_ok=True, parents=True)
            setattr(
                    namespace,
                    'frames_directory',
                    output_directory / 'frames',
                    )
            namespace.frames_directory.mkdir(exist_ok=True, parents=True)


class CacheDirectoryAction(argparse.Action):
    def __call__(self, parser, namespace, cache_directory, option_string=None):
        _tmp_parser = argparse.ArgumentParser(add_help=False)
        _tmp_parser.add_argument('run_directory', type=Path)
        _tmp_parser.add_argument('variable')
        _tmp_parser.add_argument('--output-directory', '-o', required=True, type=Path, action=OutputDirectoryAction)
        known, _ = _tmp_parser.parse_known_args()
        cache_directory.mkdir(exist_ok=True, parents=True)
        setattr(namespace, self.dest, cache_directory)
        setattr(
                namespace,
                'frames_directory',
                known.output_directory / 'frames',
                )
        namespace.frames_directory.mkdir(exist_ok=True, parents=True)


class LevelsAction(argparse.Action):
    def __call__(self, parser, namespace, values, option_string=None):
        if ',' in values:
            values_list = values.split(',')
            try:
                levels = [float(val) for val in values_list]
            except ValueError:
                parser.error("Invalid list of floats: '{}'".format(values))
        else:
            try:
                levels = int(values)
            except ValueError:
                parser.error("Invalid integer: '{}'".format(values))
        setattr(namespace, self.dest, levels)


class BboxAction(argparse.Action):

    def __call__(self, parser, namespace, values, option_string=None):

        _tmp_parser = argparse.ArgumentParser(add_help=False)
        _tmp_parser.add_argument('run_directory', type=Path, action=RunDirectoryAction)
        _tmp_parser.add_argument('variable')
        _tmp_parser.add_argument('--output-directory', '-o', type=Path, required=True, action=OutputDirectoryAction)
        _tmp_parser.add_argument('--cache-directory', type=Path, action=CacheDirectoryAction)
        _tmp_parser.add_argument('--xmin', type=float)
        _tmp_parser.add_argument('--ymin', type=float)
        _tmp_parser.add_argument('--xmax', type=float)
        _tmp_parser.add_argument('--ymax', type=float)
        _tmp_parser.add_argument(
                "--log-level",
                choices=["warning", "info", "debug"],
                default="warning",
            )

        known, unknow = _tmp_parser.parse_known_args()

        init_logger(known.log_level)
        if isinstance(getattr(namespace, self.dest), Bbox):
            return

        hgrid = load_hgrid_from_args(MPI.COMM_WORLD, known)

        hgrid_bbox = hgrid.get_bbox()

        xmin = hgrid_bbox.xmin if known.xmin is None else known.xmin
        ymin = hgrid_bbox.ymin if known.ymin is None else known.ymin
        xmax = hgrid_bbox.xmax if known.xmax is None else known.xmax
        ymax = hgrid.bbox.ymax if known.ymax is None else known.ymax

        setattr(namespace, self.dest, Bbox.from_extents([xmin, ymin, xmax, ymax]))


def get_argument_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument('run_directory', type=Path, action=RunDirectoryAction)
    parser.add_argument('variable')
    parser.add_argument('--output-directory', '-o', type=Path, required=True, action=OutputDirectoryAction)
    parser.add_argument('--cache-directory', type=Path, action=CacheDirectoryAction)
    parser.add_argument('--start-step', type=int)
    parser.add_argument('--end-step', type=int)
    parser.add_argument('--xmin', type=float, action=BboxAction, dest='bbox')
    parser.add_argument('--ymin', type=float, action=BboxAction, dest='bbox')
    parser.add_argument('--xmax', type=float, action=BboxAction, dest='bbox')
    parser.add_argument('--ymax', type=float, action=BboxAction, dest='bbox')
    parser.add_argument('--alpha', type=float, help='Alpha value')
    parser.add_argument('--vmin', type=float, help='Vmin value')
    parser.add_argument('--vmax', type=float, help='Vmax value')
    parser.add_argument('--basemap_zoom', type=int, help='Basemap zoom level')
    parser.add_argument('--dpi', type=int, help='Dots per inch (DPI)', default=100.)
    parser.add_argument(
            '--figsize',
            nargs=2,
            type=float,
            help='Figure size (width, height) in inches',
            default=[19.2, 10.8]
            )
    parser.add_argument(
            '--context',
            type=str,
            help='Set context',
            default='poster',
            choices=['paper', 'notebook', 'talk', 'poster']
            )
    parser.add_argument('--cmap', default='jet', help='Colormap')
    parser.add_argument(
            '--levels',
            default=256,
            type=str,
            action=LevelsAction,
            help='Levels argument: single integer or a list of floats'
            )
    parser.add_argument('--overwrite', action='store_true')
    parser.add_argument(
            "--log-level",
            choices=["warning", "info", "debug"],
            default="warning",
        )
    parser.add_argument('--stations', type=str, help='Stations information')
    parser.add_argument(
            '--auto-stations',
            type=Path,
            )
    parser.add_argument('--fps', type=float, default=6.)
    parser.add_argument('--fourcc', type=lambda x: cv2.VideoWriter_fourcc(*x))
    parser.add_argument('--extension', choices=['mp4', 'mov', 'avi'], default='mp4')
    parser.add_argument('--mode', choices=['combined', 'individual', 'both'], default='individual')
    return parser


# def main():
#     sys.excepthook = mpiabort_excepthook
#     comm = MPI.COMM_WORLD
#     try:
#         args = get_argument_parser().parse_args()
#     except SystemExit:
#         comm.Abort(-1)
#     comm.barrier()
#     args = comm.bcast(args, root=0)
#     init_logger(args.log_level)
#     entrypoint(args, comm=comm)


if __name__ == "__main__":
    args = get_argument_parser().parse_args()
    init_logger(args.log_level)
    entrypoint(args)
    # main()
